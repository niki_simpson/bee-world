/* eslint-disable react-hooks/exhaustive-deps */
import { useCallback } from "react";
import { aeFlag, bgFlag } from "../../assets";
import { useDispatch } from "react-redux";
import { changeLanguage } from "../../reducers";
import styled from "styled-components";

const TopNavbar = () => {
  const dispatch = useDispatch();

  const changeLang = useCallback((lang) => {
    dispatch(changeLanguage(lang));
  }, []);

  return (
    <NavbarContainer>
      <NavbarInnerContainer>
        <FlagsWrapper>
          <Flags onClick={() => changeLang("bg")} src={bgFlag} />/
          <Flags onClick={() => changeLang("ae")} src={aeFlag} />
        </FlagsWrapper>
        <NavbarCenterContainer>
          <div>Бззззз... Пчеличката приема поръчки денонощно!</div>
        </NavbarCenterContainer>
        <TextWrapper>
          Но, отговаря на телефон + 359 88622 6429 от 9:00 до 18:00
        </TextWrapper>
      </NavbarInnerContainer>
    </NavbarContainer>
  );
};
export default TopNavbar;

const NavbarContainer = styled.nav`
  display: flex;
  flex-direction: row;
  width: 100%;
  background-color: black;
  color: #f9d40d;
  height: 24px;
  font-size: 1em;
`;

const NavbarInnerContainer = styled.div`
  display: flex;
  width: 100%;
  height: 26px;
  background-color: black;
  position: fixed;
  z-index: 100;
`;

const NavbarCenterContainer = styled.div`
  flex: 60%;
  display: flex;
  position: relative;
  align-items: center;
  justify-content: flex-start;
  left: 2%;
`;

const TextWrapper = styled.div`
  position: relative;
  left: -2%;
`;

const FlagsWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  position: relative;
  top: -3px;
  padding-right: 2px;
  z-index: 1;
`;

const Flags = styled.img`
  width: 30px;
  height: 30px;
  cursor: pointer;
`;
