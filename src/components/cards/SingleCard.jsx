import styled from "styled-components";
import { zoomMixin } from "../../utils";

const SingleCard = () => {
  return (
    <Wrapper>
      <CardImage src="https://honeymamas.com/cdn/shop/files/Manuka-Hero-Mobile-1_1500x.jpg?v=1705619263" />
      <ProductInfo>
        <ProductTitle>Harvest Vase</ProductTitle>
        <SecondTitle>by studio and friends</SecondTitle>
        <ProductContent>
          Harvest Vases are a reinterpretation
          <br /> of peeled fruits and vegetables as
          <br /> functional objects. The surfaces
          <br /> appear to be sliced and pulled aside,
          <br /> allowing room for growth.
        </ProductContent>
        <ProductButtonWrapper>
          <PriceWrapper>
            <ProductPrice>78</ProductPrice>$
          </PriceWrapper>
          <ProductButton type="button">buy now</ProductButton>
        </ProductButtonWrapper>
      </ProductInfo>
    </Wrapper>
  );
};
export default SingleCard;

const Wrapper = styled.div`
  ${({ theme }) =>
    zoomMixin(
      theme.isDesktop ? 554 : theme.isLandscape ? 420 : 420,
      theme.isDesktop ? 340 : theme.isLandscape ? 654 : 654
    )};
  display: flex;

  margin-bottom: 2%;
  font-family: "Bentham", serif;
  border-radius: 7px 7px 7px 7px;
  box-shadow: 0px 14px 32px 0px rgba(0, 0, 0, 0.15);
  background-color: #fff;
`;

const CardImage = styled.img`
  ${({ theme }) =>
    zoomMixin(
      theme.isDesktop ? 247 : theme.isLandscape ? 420 : 420,
      theme.isDesktop ? 340 : theme.isLandscape ? 654 : 654
    )};
  border-radius: 7px 0 0 7px;
`;

const ProductInfo = styled.div`
  ${({ theme }) =>
    zoomMixin(
      theme.isDesktop ? 307 : theme.isLandscape ? 420 : 420,
      theme.isDesktop ? 340 : theme.isLandscape ? 654 : 654
    )};
  border-radius: 0 7px 10px 7px;
`;

const ProductTitle = styled.div`
  margin-left: 10px;
  font-weight: bold;
  font-size: 34px;
  color: #474747;
`;

const SecondTitle = styled.div`
  margin: 0 0 5px 20px;
  font-size: 13px;
  font-family: "Raleway", sans-serif;
  font-weight: 400;
  text-transform: uppercase;
  color: #d2d2d2;
  letter-spacing: 0.2em;
`;

const ProductContent = styled.div`
  margin: 10px 0 5px 10px;
  font-family: "Playfair Display", serif;
  color: #8d8d8d;
  line-height: 1.7em;
  font-size: 18px;
  font-weight: lighter;
  overflow: hidden;
`;

const ProductButtonWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  position: relative;
  top: 20px;
`;

const PriceWrapper = styled.div`
  font-family: "Trocchi", serif;
  font-size: 28px;
  font-weight: lighter;
  color: #474747;
`;

const ProductPrice = styled.div`
  display: inline-block;
  font-family: "Suranna", serif;
  font-size: 34px;
`;

const ProductButton = styled.button`
  ${({ theme }) =>
    zoomMixin(
      theme.isDesktop ? 176 : theme.isLandscape ? 420 : 420,
      theme.isDesktop ? 50 : theme.isLandscape ? 654 : 654
    )};
  border: transparent;
  border-radius: 60px;
  font-family: "Raleway", sans-serif;
  font-size: 16px;
  font-weight: 500;
  text-transform: uppercase;
  letter-spacing: 0.2em;
  color: #ffffff;
  background-color: #9cebd5;
  cursor: pointer;
  outline: none;

  &:hover {
    background-color: #f9d40d;
  }
`;
