import styled from "styled-components";
import SingleCard from "./SingleCard";

const HomeCards = () => {
  return (
    <Background>
      <BackgroundColor>
        <Wrapper>
          <SingleCard />
          <SingleCard />
          <SingleCard />
          <SingleCard />
          <SingleCard />
          <SingleCard />
        </Wrapper>
      </BackgroundColor>
    </Background>
  );
};
export default HomeCards;

const Background = styled.div`
  display: flex;
  background: url(https://png.pngtree.com/background/20230526/original/pngtree-golden-and-black-honeycomb-structure-in-the-shape-of-honeycombs-picture-image_2748329.jpg)
    center center no-repeat;
  background-size: cover;
  background-attachment: fixed;
`;

const BackgroundColor = styled.div`
  background-color: rgb(0 0 0 / 37%);
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  flex-wrap: wrap;
  margin-top: 18%;
`;
