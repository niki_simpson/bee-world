import { useWindowSize } from "./utils";
import { BottomTopNavbar, NavBar, TopNavbar } from "./components";
import { HomeView } from "./views";

import { ThemeProvider } from "styled-components";
import "./index.css";
import { Navigate, Route, Routes } from "react-router-dom";

function App() {
  const theme = useWindowSize();

  return (
    <ThemeProvider theme={theme}>
      <TopNavbar />
      <NavBar />
      <BottomTopNavbar />
      <Routes>
        <Route index element={<Navigate replace to="home" />} />
        <Route path="/home" element={<HomeView />} />
      </Routes>
    </ThemeProvider>
  );
}

export default App;
