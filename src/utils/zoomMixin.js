import { css } from "styled-components";

export const zoomMixin = (designWidth, designHeight) => {
  // WARNING: Can't use margins on zoomed element (margin: auto atuo will not work). Wrap in a div with desired margins.
  return css`
    width: ${designWidth}px;
    height: ${designHeight}px;
    zoom: var(--scale-coef);

    @supports not (zoom: var(--scale-coef)) {
      --marginLR: calc(${designWidth}px * (var(--scale-coef) - 1) / 2);
      --marginTB: calc(${designHeight}px * (var(--scale-coef) - 1) / 2);
      transform: scale(var(--scale-coef));
      transform-origin: center center;

      margin: var(--marginTB) var(--marginLR) var(--marginTB) var(--marginLR);
    }
  `;
};
