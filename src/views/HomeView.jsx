import styled from "styled-components";
import { HomeCards } from "../components";

const HomeView = () => {
  return (
    <Wrapper>
      <HomeCards />
    </Wrapper>
  );
};
export default HomeView;

const Wrapper = styled.div`
  margin-top: -18%;
`;
